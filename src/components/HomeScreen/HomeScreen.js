import React, { Component } from 'react';
import { StyleSheet, View, StatusBar, Image } from 'react-native';
import {
    Container,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Button,
    Body,
    Content,
    Text,
    Card,
    CardItem,
    Footer,
    FooterTab
} from "native-base";

export default class HomeScreen extends Component {
    render() {
        return (
            <Container>
                <Header style={styles.header}>
                    <Left>
                        <Button><Icon name="menu" /></Button>
                    </Left>
                    <Body><Title>HomeScreen</Title></Body>
                    <Right />
                </Header>
                <Content padder>
                    <Card>
                        <CardItem>
                            <Left>
                                <Image source={{ uri: "https://cdn-images-1.medium.com/max/1200/1*ub1DguhAtkCLvhUGuVGr6w.png" }} style={{ width: 120, height: 120, resizeMode: 'contain' }} />
                            </Left>
                            <Body><Text>Chat App to talk some awesome people!</Text></Body>
                        </CardItem>
                    </Card>
                    <Button full rounded primary
                        style={{ marginTop: 10 }}
                    >
                        <Text>Go to Profile</Text>
                    </Button>
                </Content>
                <Footer>
                    <FooterTab>
                        <Button vertical>
                            <Icon name="bowtie" />
                            <Text>Lucky</Text>
                        </Button>
                        <Button vertical>
                            <Icon name="headset" />
                            <Text>Jade</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        paddingTop: 10
    }
});
