import React from 'react';
import { StyleSheet, View } from 'react-native';
import HomeScreen from "./src/components/HomeScreen/HomeScreen"
import { Font, AppLoading } from "expo";

export default class App extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            loading: true
        }
    }

    async componentWillMount() {
        await Font.loadAsync({
            Roboto: require("native-base/Fonts/Roboto.ttf"),
            Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
            Ionicons: require("native-base/Fonts/Ionicons.ttf")
        })
        this.setState({loading: false})
    }

    render() {
        if (this.state.loading) {
            return <AppLoading />
        }

        return (
            <HomeScreen />
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
